[toc]

# interop
Module concernant l'interopérabilité avec d'autre site 

`interopObj` regroupes les parametres, et les fonctions pour rechercher la donné sur une db

`interop` : Une Obj qui va executer la recherche en fonction des parametres et function de interopObj

`interop.currentType : []`, tableau des db qu'on interoge.

Quand on lance le search

- startSearch() : directory.js 
    - interopSearch(keyS, nameS) : search.js ` Charge les fichiers js de interops si l'objet interop n'existe pas et lance la recherche `
        - interopObj[keyS].startSearch(searchObject.indexMin, searchObject.indexStep) ` keyS correspond à la db que tu veut intéroger`
            - interop.searchStart() `init les parametres `
                - interop.getUrlForInteropResearch(); ` parcourir les db qu'on soite interrogé et lance la recherche `
                    - interopObj[keyS].getParamsUrl ` Charge les parametres de l'url `
                    + interopObj[keyS].getUrlApi `récupere l'url `
                    + interop.getResults() ``` exexute l'url en ajax ```
                        * directory.interopPanelHtml(value, objType); affiche le resultat ``` dans interop.js ```

# Mediawiki 

## MediawikiController

### /interop/mediaWiki/index

Si pas de mediawiki connécté =>
   `return $this->renderPartial("interop.views.create.index");` =>
dynForm insertWikiForm (assets/forms/insertWiki.js).  => 
**/interop/mediaWiki/chooseCategory** qui insert nom du wiki et url du wiki en db

Sinon
`return $this->renderPartial("interop.views.default.indexMediaWiki");`

### /interop/mediaWiki/chooseCategory
Ici on vérifie qu'un médiawiki avec l'url rentré par l'utilisateur n'existe pas deja en db 
`return $this->renderPartial("interop.views.create.index");`
dynForm insertCatWikiForm (assets/forms/chooseCat.js) => 
**/interop/mediaWiki/insertCat** qui insert le choix de l'user des catégorie en fonctions des élément acteurs(person, organization), ressources, projets.

Sinon
`return $this->renderPartial("interop.views.default.indexMediaWiki");`

### /interop/mediaWiki/menuLeft
Comme son nom l'indique cette route s'occupe de chercher la data pour le menuleft
et renvoie la vue avec les differente page en accordeon dans menu left.
`return $this->renderPartial("interop.views.menus.pages");`

### /interop/mediaWiki/page
Cette fois c'est pour la data d'une page spécifique.
Tout un process est en route dans les models afin de cherché la data
sur le wiki et la trier pour la rendre a la celon toujours le méme schema.
Un tour est fait dans citizenToolKit pour le convert et translate de la data du wiki.
`return $this->renderPartial("interop.views.menus.pages");`

### /interop/mediaWiki/edit
Ici la route est appelé en cas de page communecter au méme nom que sur le wiki,
et choix de l'user de marquer la page du wiki ce cette présence.

!! Attention ici un réglage sur le wiki est a faire afin d'intégrer 
la propriétés   `|pageCo=` dans le modéle des pages du wiki.
renvoi une string message sur le succés ou non de l'opération