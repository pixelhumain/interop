<?php

/**
 * @author: Després Laurent <laurentd@netc.fr>
 * date: 04/2020
 *
 * \brief     This is the welcome page of the module interop for mediawiki
 *
 * Contains Html and script js
 * call asset /css/styleInteropMod.css, /js/interop.js, /js/welcomePage.js
 */
$cssAnsScriptFilesModule = array(
    '/js/dataHelpers.js');
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule(Yii::app()->params["module"]["parent"])->getAssetsUrl());
$cssAnsScriptFilesModule = array(
    '/css/mediawiki.css',
    '/js/interop.js',
    '/js/welcomePage.js',
    //'/js/menuLeft.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule(Interop::MODULE)->getAssetsUrl());
 // var_dump($wikiParams);exit;
//var_dump(['test vue indexmedia' => $wikiParams]);exit;
if (isset($wikiParams['error'])) {
    echo '<p class="error-message">' . $wikiParams['error'] . '</p><br/>
		<a href="/" class="error-message" title="Accueil" alt="Accueil" style="width: inherit !important;text-transform: capitalize;">
		<i class="fa fa-home" class="error-message"></i> <span class="hidden-xs hidden-sm" style="font-size: 16px;">Accueil</span>';
} else {
    ?>
<!-- 	*************************************************
		header mediawiki
		************************************************* -->
	<div id="header-interop" class="col-xs-12">
		<div id="title-and-search col-xs-12">
		<?php
if ($wikiParams['wiki']['params'] == "none") {
        ////////////TODO: ecrire code si pas trouver de categories
    } else {
		$count = count($wikiParams['wiki']['params']);
		$xsParam = 6;
		if ($count > 2) {
			$xsParam = 3;
		}
        echo '<h4 class="col-xs-6 elipsis no-margin title-interop col-xs-5" style="color:#65BA91">' . Yii::t("docs", "  MediaWiki ") . '<i class="fa fa-at"></i><a style="color:#65BA91" href="/#@' . $_POST['name'] . '">' . $_POST['name'] . '</a></h4>';
        ?>
			<div id="search-bar-wiki" class="col-xs-5">
				<input id="text-search-wiki" type="text" class="text-center" placeholder="Trouver une page du wiki?">
				<button class="btn btn-default bold" type="button" id="search-in-wiki" data-id="<?=$wikiParams['id']?>" data-name="<?=$wikiParams['wiki']['name']?>" data-categorie="search"><i type="button" class="fa fa-arrow-circle-right"></i></button>
			</div>
		</div>
		<ul id="button-bar" class="nav col-xs-12">
			<?php 
			foreach ($wikiParams['wiki']['params'] as $key => $value) {
            $text = ($key === 'actors') ? "acteurs" : "";
            $text = ($key === 'classifieds') ? "ressources" : $text;
            $text = ($key === 'projects') ? "projets" : $text;
            echo '<li class="nav-item col-xs-'.$xsParam.'">
						<a class="bold btn-directory-categorie"  data-id="' . $wikiParams['id'] . '" data-name="' . $wikiParams['wiki']['name'] . '" data-categorie="' . $key . '">' . $text . '</a>
					</li>';
        }
        echo '<li class="nav-item col-xs-2">
						<a id="close-interop">retour</a>
					</li>';
        ?>
		</ul>
		<?php
}
    ?>
	</div>
	<div id="menu-left" class="col-xs-3 shadow2 no-padding">
	<!-- 	*************************************************
		menuLeft mediawiki
		************************************************* --></div>
		<div id="loading-menu" class="col-xs-3 shadow2 no-padding">
			<p></p>
			<ul>
			<?php
echo '			<li>
					<a href="javascript:;">~</a>
				</li>
				<li>
					<a href="javascript:;">loading</a>
				</li>
				<li>
					<a href="javascript:;">~</a>
				</li>';
    ?>
			</ul>
		</div>
		<div class="col-xs-9">
			<div id="loading-page" class="col-xs-12 shadow2 no-padding">
				<p></p>
				<ul>
				<?php
	echo '			<li>
						<a href="javascript:;">~</a>
					</li>
					<li>
						<a href="javascript:;">loading</a>
					</li>
					<li>
						<a href="javascript:;">~</a>
					</li>';
		?>
				</ul>
			</div>
		</div>
		<div id="block-interop" class="col-xs-9">
	<!-- 	*************************************************
		content mediawiki
		************************************************* -->
	</div>
<?php
}
?>