<?php

/**
 * @author: Després Laurent <laurentd@netc.fr>
 * date: 04/2020
 * 
 * \brief 	This is the welcome page of the module interop
 * 			
 * Contains Html and script js
 * call asset /css/styleInteropMod.css, /js/interop.js, /js/docInterop.js
 * dropdown menu are automatic related with actions of defaultController
 * and contains logo(path of logo in action to)
 */
$cssAnsScriptFilesModule = array(
	'/js/dataHelpers.js',
	'/js/interoperability/interoperability.js',

);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule(Yii::app()->params["module"]["parent"])->getAssetsUrl());

$cssAnsScriptFilesModule = array(
	'/plugins/jquery-simplePagination/jquery.simplePagination.js',
	'/plugins/jquery-simplePagination/simplePagination.css',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getRequest()->getBaseUrl(true));
$cssAnsScriptFilesModule = array(
	'/css/mediawiki.css',
	'/js/interop.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule(Interop::MODULE)->getAssetsUrl());
var_dump($this->dataView);exit;
?>
<div id="header-interop">
	<a href='#' id="show-menu-xs" class="visible-xs visible-sm pull-left" data-placement="bottom" data-title="Menu"><i class="fa fa-bars"></i></a>
	<h2 class="elipsis no-margin"><span style="color:#65BA91"><?php echo Yii::t("docs", "  interopérabilité") ?> </span></h2>
	<a href='#' class="lbh pull-right" id="close-docs"><span><i class="fa fa-sign-out"></i> <?php echo Yii::t("common", "Back") ?></span></a>
	<br />
</div>
<div class="col-md-12 col-sm-12 col-xs-12" style="display: flex; justify-content: space-around; font-size: 2em;">


	<div class="dropdown bt-interop">
		<button class="btn btn-primary dropdown-toggle interopMod" type="button" id="dropdownProjets" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<i class="fa fa-angle-right"></i>Projets
		</button>
		<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownProjets">
			<?php foreach ($this->actions as $key => $value) {
				if (in_array("Projets", $value["themes"])) {
					echo '
						<button class=" dropdown-item btn-directory-categorie" data-media="' . $value['mediaWiki'] . '" data-type="' . $key . '" data-categorie="Projets" type="button">
							  <img src="' . $this->module->assetsUrl . '/images/logos/' . $value['logo'] . '" style="width:100px;height:70px;" alt="logo"></button>';
				}
			} ?>
		</div>
	</div>

	<div class="dropdown bt-interop">
		<button class="btn btn-primary dropdown-toggle interopMod" type="button" id="dropdownActeurs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<i class="fa fa-angle-right"></i>Acteurs
		</button>
		<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownActeurs">
			<?php foreach ($this->actions as $key => $value) {
				if (in_array("Acteurs", $value["themes"])) {
					echo '
						<button class=" dropdown-item btn-directory-categorie" data-media="' . $value['mediaWiki'] . '" data-type="' . $key . '" data-categorie="Acteurs" type="button">
						<img src="' . $this->module->assetsUrl . '/images/logos/' . $value['logo'] . '" style="width:100px;height:70px;" alt="logo"></button>';
				}
			} ?>
		</div>
	</div>

	<div class="dropdown bt-interop">
		<button class="btn btn-primary dropdown-toggle interopMod" type="button" id="dropdownEmplois" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<i class="fa fa-angle-right"></i>Emplois
		</button>
		<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownEmplois">
			<?php foreach ($this->actions as $key => $value) {
				if (in_array("Emplois", $value["themes"])) {
					echo '
						<button class=" dropdown-item btn-directory-categorie" data-media="' . $value['mediaWiki'] . '" data-type="' . $key . '" data-categorie="Emplois" type="button">
							  <img src="' . $this->module->assetsUrl . '/images/logos/' . $value['logo'] . '" style="width:100px;height:70px;" alt="logo"></button>';
				}
			} ?>
		</div>
	</div>

	<div class="dropdown bt-interop">
		<button class="btn btn-primary dropdown-toggle interopMod" type="button" id="dropdownDocumentations" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<i class="fa fa-angle-right"></i>Documentations
		</button>
		<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownDocumentations">
			<?php foreach ($this->actions as $key => $value) {
				if (in_array("Documentations", $value["themes"])) {
					echo '
						<button class=" dropdown-item btn-directory-categorie" data-media="' . $value['mediaWiki'] . '" data-type="' . $key . '" data-categorie="Documentations" type="button">
						<img src="' . $this->module->assetsUrl . '/images/logos/' . $value['logo'] . '" style="width:100px;height:70px;" alt="logo"></button>';
				}
			} ?>
		</div>
	</div>

	<div class="dropdown bt-interop">
		<button class="btn btn-primary dropdown-toggle interopMod" type="button" id="dropdownCreate" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<i class="fa fa-angle-right"></i>Créer un document
		</button>
		<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownCreate">
			<?php foreach ($this->actions as $key => $value) {
				$categories = implode(" ", $value["themes"]);
				if ($value["create"]) {
					echo '
							  <button class="dropdown-item btn-directory-categorie" data-media="' . $value['mediaWiki'] . '" data-type="' . $key . '" data-categorie="create" data-categories="' . $categories . '" type="button">
							  <img src="' . $this->module->assetsUrl . '/images/logos/' . $value['logo'] . '" style="width:100px;height:70px;" alt="logo"></button>';
				}
			} ?>
		</div>
	</div>
</div>
<div id="block-interop"></div>
<div id="dropdown_search" class="col-md-8 col-sm-8 col-xs-10 padding-10"></div>

</div>

<div class="no-padding col-xs-12 text-left footerSearchContainer"></div>
<script type="text/javascript">
	var pageCount = false;
	jQuery(document).ready(function() {
		$(".btn-directory-categorie").click(function() {
			var type = $(this).data("type")
			var categorie = $(this).data("categorie")
			// var media = $(this).data("media")
			// if (media) {
			// 	console.log(media)
			// 	if (categorie != "create") {
			// 		interopMW.initMenu(type, categorie);
			// 	} else {
			// 		var categories = ($(this).data("categories"))
			// 		interopMW.initCreate(type, categories);
			// 	}
			// } else {
				interop.currentType = [type];
				interop.startSearch(0, 30);

			// }
		})
	});
</script>