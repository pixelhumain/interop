<?php

/**
 * docContentActeurs.php
 *
 * \brief views for mediaWiki element in interop module
 *
 * @author: Després Laurent <laurentd@netc.fr>
 * date: 04/2020
 *
 */
$cssAnsScriptFilesModule = array(
    '/js/contentPage.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule(Interop::MODULE)->getAssetsUrl());
//var_dump(['test vue page' => $dataView]);exit;
// var_dump($dataView);exit;
$pageData = $dataView['data'];
?>
<div id="pageWiki" class="col-xs-12 shadow2 no-padding">
	<div id="menu-top-page" class="col-md-12 col-sm-12 col-xs-12">
		<h3 id="nameEl" class="text-red"><?=str_replace('Utilisateur:', '', $pageData['name'])?></h3>
		<div class="nav nav-tabs col-xs-12">
		<a class="btn btn-default bold btn-actor btw nav-item" onclick="show('btw')" >A Propos</a><?php
	if (isset($pageData['communaute']) || isset($pageData['utilisateur'])) {
		echo '<a class="btn btn-default bold btn-actor community" onclick="show(\'community\')" >Communautés</a>';
	}
	if (isset($pageData['defi'])) {
		echo '<a class="btn btn-default bold btn-actor defis nav-item" onclick="show(\'defis\')" >Défis</a>';
	}
	if (isset($pageData['ressource'])) {
		echo '<a class="btn btn-default bold btn-actor resource nav-item" onclick="show(\'ressources\')" >Ressources</a>';
	}
	?>
	</div>


	<div id="body-actors" class="col-xs-12">
		<div id="btw" class=" col-xs-12 c-actor ">
			<div class="card col-xs-12">
					<?php
	if ($pageData['image'] != "none") {
		echo '<img id="imageEl" src="' . $pageData['image'] . '"class=" card-img-top" alt="logo" style="width:300px;height:150px;">';
	}
	?>
				<div class="card-header">
					<span id="typeEl" data-type="<?=$pageData['type']?>">
						<?php
	// if (isset($pageData['@type'])) {
	// 	if ($pageData['@type'] === 'Classifieds') {
	// 		echo "Ressource";
	// 	} else {
	// 		echo $pageData['@type'];
	// 	}
	// }
	?>
					</span>
				</div>
				<div class="card-body interopMod col-xs-12">
					<?php
	if ($pageData['data']) {
		if (isset($pageData['tags'])) {
			foreach ($pageData['tags'] as $badgeG) {
				echo '
							<span class="badge tagsEl badge-primary">' . $badgeG . '</span>';
			}
		}
		if (isset($pageData['description'])) {
			echo '<p class="text">' . $pageData['description'] . '</p>';
			if (isset($pageData['shortDescription'])) {
				echo
					'<p id="shortEl" class="hidden">' . $pageData['shortDescription'] . '</p>';
			}
		} else {
			if (isset($pageData['shortDescription'])) {
				echo '<p id="shortEl" class="text">' . $pageData['shortDescription'] . '</p>';
			}
			////need mapping
			// if (isset($pageData['Giving'])) {
			//     echo '<p class="text">' . $pageData['Giving'][0]['fulltext'] . '</p>';
			// }
			// if (isset($pageData['Needs'])) {
			//     echo '<p class="text">' . $pageData['Needs'][0]['fulltext'] . '</p>';
			// }
		}
		if (!empty($pageData['etatProjet'])) {

			echo '<span class="text">Etat du projet: <strong>' . implode(", ", $pageData['etatProjet']) . '</strong></span><br/>';
		}
		if (!empty($pageData['country'])) {

			echo '<span class="text">Pays d\'implantations: <strong>' . implode(", ", $pageData['country']) . '</strong></span><br/>';
		}
		if (!empty($pageData['city'])) {
			if (gettype($pageData['city']) == "array") {
				$city = implode(", ", $pageData['city']);
			} else {
				$city = $pageData['city'];
			}
			echo '<span class="text">Ville d\'implantations: <strong>' . $city . '</strong></span><br/>';
		}
		echo '<br />';
		if (!empty($pageData['themes'])) {
			foreach ($pageData['themes'] as $badgeS) {
				if (isset($pageData['tags']) && !in_array($badgeS, $pageData['tags'])) {
					echo '
							<span class="badge badge-secondary">' . $badgeS . '</span>';
				} else {
					echo '
							<span class="badge badge-secondary">' . $badgeS . '</span>';
				}
			}
		}
		//////////////////Pour les utilisateurs
		if (isset($pageData['themeUt'])) {
			echo '<h4>Thémes associés :</h4><p>';
			$str = "";
			foreach ($pageData['themeUt'] as $k => $t) {
				$str = $str . $t . ",";
			}
			echo substr($str, 0, -1) . "</p>";
		}
		echo '<br />';
		if (!empty($pageData['skill'])) {
			//var_dump($pageData['skill']);exit;
			echo '<h4>Compétences :</h4>';
			$str = "";
			foreach ($pageData['skill'] as $s) {
				$str = $str . $s['name'] . ",";
			}
			echo substr($str, 0, -1) . "</p>";
		}
		echo '
					</p>';
	} else {
		echo '
					<p> Aucune données n\'est accessible via l\'api du médiawiki </p>';
	}
	?>
					<a href="<?=$pageData['urls']['wiki']?>" target="_blank" class="btn btn-default">Page du wiki</a>
					<?php
	if (isset($pageData['urls']['webSite'])) {
		echo '<a id="urlEl" href="' . $pageData['urls']['webSite'] . '" target="_blank" class="btn btn-default">Site Web</a>';
	}
	if (isset($pageData['slug']) && !isset($pageData['coInWiki'])) {
		if (isset($pageData['slug'])) {
			echo '<a href="/#@' . $pageData["slug"] . '" target="_blank" class="btn btn-default">Page Communecter</a>
							<a id="edit-wiki" data-page="' . $pageData["name"] . '" data-slug="' . $pageData['slug'] . '">Marquer le wiki de la présence d\'une page communecter</a>';
		}
	} else {
		if ($pageData['type'] != "citoyen") {
			if ($pageData['@type'] == "Classifieds") {
				$formType = "ressources";
			} else {
				$formType = lcfirst($pageData['@type']);
			}
			//var_dump($formType); exit;
			echo '<a id="create-in-co" class="btn btn-default" data-type="' . $formType . '">Créer dans Communecter</a>';
		}
	}
	?>
					<div id="mark-on-wiki"></div>
				</div>
			</div>
		</div>
		<div id="community" class="panel-group c-actor col-xs-12">
			<?php
	if (isset($pageData['communaute']) || isset($pageData['acteur'])) {
		?>
				<div class="card card-community panel panel-default col-xs-12">
					<div class="panel-heading">
						<div class="card-header panel-title">
							<button class="btn btn-default btn-lg btn-block" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true">Communautés en relations avec <?=$pageData['name']?></button>
						</div>
					</div>

					<div id="collapseOne" class="collapse in">
						<ul>
							<?php
	if (isset($pageData['communaute'])) {
			foreach ($pageData['communaute'] as $v) {
				echo '<li><button type="button" class="btn btn-default btn-sm" data-categorie="acteurs">' . $v['name'] . '</button>';
			}
		}
		if (isset($pageData['acteur'])) {
			foreach ($pageData['acteur'] as $v) {
				echo '<li><button type="button" class="btn btn-default btn-sm" data-categorie="acteurs">' . $v['name'] . '</button>';
			}
		}
		?>
						</ul>
					</div>

				<?php
	}
	if (isset($pageData['utilisateur'])) {
		?>

					<div class="panel-heading">
						<div class="card-header panel-title">
							<button class="btn btn-default btn-lg btn-block" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true">Personnes impliquées</button>

						</div>
					</div>
					<div id="collapseTwo" class="collapse in">
						<ul>
							<?php
	foreach ($pageData['utilisateur'] as $v) {
			$name = str_replace('Utilisateur:', '', $v['name']);
			echo '<li><button type="button" class="btn btn-default btn-sm" data-categorie="utilisateurs">' . $name . '</button>';
		}
		?>
						</ul>
					</div>
				</div>
			<?php
	}
	?>
		</div>
	</div>
	<div id="defis" class="c-actor col-xs-12 shadow2 no-padding">
		<div class="card card-defis col-xs-12">
			<div class="card-header panel-title">
				<h3>Défi(s) adressé(s)</h3>
			</div>
			<div class="card-body card-defis">
				<?php
	if (isset($pageData['defi'])) {
		?>
					<ul>
						<?php
	foreach ($pageData['defi'] as $v) {
			echo '<li><button type="button" class="btn btn-default btn-sm" data-categorie="projets">' . $v['name'] . '</button>';
		}
		?>
					</ul>
				<?php
	}
	?>
			</div>
		</div>
	</div>
	<div id="ressources" class=" c-actor col-xs-12 shadow2 no-padding">
		<div class="card card-ressources col-xs-12">
			<div class="card-header panel-title">
				<?php
	if (isset($pageData['ressource'])) {
		?>
					<h3>Ressources utilisées</h3>
			</div>
			<div class="card-body card-defis">
				<ul>
					<?php
	foreach ($pageData['ressource'] as $v) {
			echo '<li><button type="button" class="btn btn-default btn-sm" data-categorie="ressources">' . $v['name'] . '</button>';
		}
		?>
				</ul>
			<?php
	}
	?>
			</div>
		</div>
	</div>
</div>