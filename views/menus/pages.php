<?php

/**
 * menuDoc.php
 *
 * \brief views for menuDoc
 *
 * @author: Després Laurent <laurentd@netc.fr>
 * date: 04/2020
 * 
 * contains html and 
 */
$cssAnsScriptFilesModule = array(
	'/js/menuLeft.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule(Interop::MODULE)->getAssetsUrl());
//var_dump(["test vue menuleftfirst" => $dataView]);exit;
?>

	<h4 id="title-menu" class=" text-red"><?= strtoupper($dataView['categorie']) ?></h4>
	<ul class="subMenu col-xs-12 no-padding">
		<?php
		if (count($dataView['menu']) > 30) {
			$menuSort = [];

			foreach ($dataView['menu'] as $k => $v) {
				$f = substr($k, 0, 1);
				if (array_key_exists($f, $menuSort)) {
					$menuSort[$f] += [$k => $v];
				} else {
					$menuSort[$f] = [$k => $v];
				}
			};
			echo '<div id="accordion" class="panel-group">';
			foreach ($menuSort as $k => $v) {
				echo '
					  <li class="link-docs-menu panel-title panel panel-default" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" data-target="#collapse' . $k . '">' . $k . '
					<div id="collapse' . $k . '" data-parent="#accordion" class="collapse">
					  <ul class="list-group">';
				foreach ($v as $k2 => $v2) {
					echo '<li class="list-group-item"><a href="javascript:;" class="link-page" data-id="' . $dataView['id'] . '" data-name="' . $dataView['wiki']['name'] . '" data-categorie="' . $dataView['categorie'] . '" data-page="' . $k2 . '">' . $k2 . '</a></li>';
				}
				echo '
				</li></ul>
					 </div>';
			}
		} else {
			foreach ($dataView['menu'] as $key => $value) {


				echo '			<li class="link-search-menu col-xs-12 no-padding">
								<a href="javascript:;" class="link-docs-menu link-page" data-id="' . $dataView['id'] . '" data-name="' . $dataView['wiki']['name'] . '" data-categorie="' . $dataView['categorie'] . '" data-page="' . $key . '">' . $key . '</a>
						</li>';
			}
		}
		?>
	</ul>
