<?php
include_once (Yii::app()->getModulePath()."/interop/.env.php");
/**
 * ApiMediaWiki
 *
 * @author: Després Laurent <laurentd@netc.fr>
 * date: 03/2020
 */

class ApiMediaWiki extends DB
{
    /**
     * getCurl
     *
     * @return array data from the wiki
     * fetch data by curl
     */
    public function getCurl()
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->url . $this->paramsGet);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_COOKIESESSION, true);
        $ret = curl_exec($curl);
        $dataDecode = json_decode($ret, true);
        curl_close($curl);
        return $dataDecode;
    }
    /**
     * post
     *
     * @param  array $data
     * @return array
     */
    protected function post($data)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_COOKIEJAR, Yii::app()->getModulePath() . "/interop/cookie.txt");
        curl_setopt($curl, CURLOPT_COOKIEFILE, Yii::app()->getModulePath() . "/interop/cookie.txt");
        // curl_setopt($curl, CURLOPT_COOKIESESSION, true);
        $ret = curl_exec($curl);
        curl_close($curl);
        $dataDecode = json_decode($ret, true);
        return $dataDecode;
    }
    /////////////////////////////
    //PROPERTIES
    /**
     * @var string
     */
    ///////////////////////////
    //////énorme TODO gestion des mot de passe en db ou non en variable d'environement ou autres?
    //////mot de passe au nom de co ou faire entrées les données par l'user
    ////////////////////////////
    public $paramsGet = ""; //! set this before get data
    public $properties = "";
    protected $url = "";
    public $currentCat = ""; //! set at __construct
    /**
     * @var array
     */public $dbWiki;
    public $currentWiki;
    /////////////////////////////
    /////////////////////////////
    //METHODES
    /**
     * __construct
     *
     * @return void
     * create parent
     * with parentId parentName arentType for create wiki step else by id
     *
     * set categorie if is set
     */
    public function __construct()
    {

        //var_dump(["test apimedia construct" => [$_POST]]);exit;
        $this->dbWiki = parent::__construct($_POST['id']);
        //var_dump(["test apimedia construct \$this->dbWiki" => [$this->dbWiki]]);exit;
        if (isset($_POST['categorie'])) {
            $this->setCurrentCat($_POST['categorie']);
        }
    }
    /**
     * insertFirstStep
     *
     * @param  string $url
     * @param  string $name
     * @return array $data['query']['allcategories']
     * set name and url of the médiawiki then fetch sorted catégories from the wiki
     */
    public function insertFirstStep($url, $name)
    {
        if (substr($url, -1) === '/') {
            $url = $url . 'api.php';
        } else {
            $url = $url . '/api.php';
        }
        $this->dbWiki['wiki']['name'] = $name;
        $this->dbWiki['wiki']['url'] = $url;
        $this->setParamsGet('?action=query&list=allcategories&aclimit=max&format=json');
        $this->url = $this->dbWiki['wiki']['url'];
        $data = $this->getCurl();
        $this->dbWiki['wiki']['params'] = $this->sortSiteCat($data['query']['allcategories']);
        $this->dbWiki = parent::saveWiki($this->dbWiki);
        //var_dump($this->dbWiki);exit;
        //var_dump(["test apimedia insertfirstste " => [$data]]);exit;
        //var_dump(['test apimedia insertfirststep 01'=> parent::saveWiki($this->dbWiki)]);exit;
    }
    /**
     * sortSiteCat
     *
     * @param  array $data
     * @return array $arrayParams = ["catActeurs" => [...], "catRessources" => [...], "catProjets" => [...]]
     * sort catégorie of mediawiki by match words (/^acteur/i, /^tiers-lieux$/i, /^ressource/i, /^commun$/i, /^projet/i)
     * TODO make array for words for match
     */
    public function sortSiteCat($data)
    {
        /// there we try to find some category that's match with own catégories
        //TODO integrate tab $wordsForRegex
        //$wordsForRegex = ["acteur", "tiers-lieux$", "ressource", "commun$", "projet"];
        $arrayParams = ["catActeurs" => [], "catRessources" => [], "catProjets" => []];
        foreach ($data as $v) {
            if (preg_match("/^acteur/i", $v['*']) && mb_stripos($v['*'], ":") === false) {
                $arrayParams["catActeurs"][] = $v['*'];
            }
            if (preg_match("/^tiers-lieux$/i", $v['*']) && mb_stripos($v['*'], ":") === false) {
                $arrayParams["catActeurs"][] = $v['*'];
            }
            if (preg_match("/^ressource/i", $v['*']) && mb_stripos($v['*'], ":") === false) {
                $arrayParams["catRessources"][] = $v['*'];
            }
            if (preg_match("/^commun$/i", $v['*']) && mb_stripos($v['*'], ":") === false) {
                $arrayParams["catRessources"][] = $v['*'];
            }
            if (preg_match("/^projet/i", $v['*']) && mb_stripos($v['*'], ":") === false) {
                $arrayParams["catProjets"][] = $v['*'];
            }
        }
        $insert = [];
        if ($arrayParams['catRessources'] != []) {
            $insert += ['ressources'  => $arrayParams['catRessources']];
        }
        if ($arrayParams['catActeurs'] != []) {
            $insert += ['acteurs'  => $arrayParams['catActeurs']];
        }
        if ($arrayParams['catProjets'] != []) {
            $insert += ['projets' => $arrayParams['catProjets']];
        }
        return $insert;
    }
    
    /**
     * insertCat
     *
     * @param  string $idWiki
     * @param  string $actors
     * @param  string $classifieds
     * @param  string $projects
     * @return array $this->dbWiki
     * Fetch state in db, create médiawiki element['params'] and save it
     */
    public function insertCat($idWiki, $actors, $classifieds, $projects)
    {
        //var_dump($this->dbWiki);exit;
        $this->dbWiki = parent::setById($idWiki);
        $dataCat = [];
        $dataCat += ["actors" => explode(',', $actors)];
        $dataCat += ["classifieds" => explode(',', $classifieds)];
        $dataCat += ["projects" => explode(',', $projects)];      
        $this->dbWiki =  parent::updateOne($idWiki, 'params', $dataCat);
    }
    /**
     * menu
     *
     * @return array
     * check category and return list of pages from wiki for menuLeft view
     */
    public function menu()
    {
        $menu = ["categorie" => $this->currentCat, "menu" => []];
        switch ($this->currentCat) {
            case 'acteurs':
                $tabParams = $this->dbWiki['wiki']['params']['actors'];
                break;
            case 'ressources':
                $tabParams = $this->dbWiki['wiki']['params']['classifieds'];
                break;
            case 'projets':
                $tabParams = $this->dbWiki['wiki']['params']['projects'];
                break;

            default:
                # code...
                break;
        }
        foreach ($tabParams as $param) {
            $this->paramsGet = "?action=ask&query=[[Cat%C3%A9gorie:" . $param . "]]|limit=500&format=json";
            $this->url = $this->dbWiki['wiki']['url'];
            $data = $this->getCurl()['query']['results'];
            foreach ($data as $key => $value) {
                $menu["menu"] += [$key => $value['fullurl']];
            }
        }
        return $menu;
    }
    /**
     * page
     *
     * @param  mixed name of the page to get data
     * @return array mapped data for the next view
     * fetch data from wiki and return data after sort with generic map
     */
    public function page($pageName)
    {
        //var_dump($this->dbWiki);exit;
        $page = [
        "title" => $pageName,
        "url" => str_replace('api.php', 'wiki/' . $pageName, $this->dbWiki['wiki']['url'])
    ];
        $validName = str_replace("&", "%26", str_replace(" ", "_", $pageName));
        $inCo = parent::findInCo($pageName);
        if ($inCo) {
            $page += ["inCo" => $inCo];
        } else {
            $page += ["inCo" => false];
        }
        $this->properties($validName);
        if ($this->properties == '') {
            $page += ["data" => false];
        } else {
            $this->paramsGet = "?action=ask&format=json&query=[[" . $validName . "]]" . $this->properties;
            $data = $this->getCurl()['query']['results'];
            if (empty($data[$page['title']]['printouts'])) {
                $page += ["data" => false];
            } else {
                $page += $data[$page['title']]['printouts'];
                $page += ["data" => true];
            }
        }
        $page += ["logo" => $this->logo($pageName)];
        $pageConvert = Convert::convertWikiMediaToPh(["data" => $page], $this->currentCat, $this->dbWiki['wiki']['name']);
        //var_dump($pageConvert['data']);exit;
        return $pageConvert;
    }
    /**
     * properties
     *
     * @param  string url encoded name of page to search data
     * @return void
     * set properties by namePage to prepare curl call
     */
    public function properties($namePage)
    {
        $this->paramsGet = "?action=browsebysubject&subject=" . $namePage . "&format=json";
        $this->url = $this->dbWiki['wiki']['url'];
        $res = $this->getCurl()['query'];
        if (isset($res['data'])) {
            foreach ($res['data'] as $value) {
                if (!preg_match("/^_/", $value['property'])) {
                    $this->properties .= "|?" . $value['property'];
                }
            }
        }
    }
    /**
     * search
     *
     * @param  string $searchText
     * @return array $menu
     * fetch list of pages on wiki by curl with search value
     *
     */
    public function search($searchText)
    {
        $menu = ["categorie" => "page pour " . $searchText, "menu" => []];
        $this->paramsGet = "?action=opensearch&search=" . $searchText . "&limit=max&format=json";
        $this->url = $this->dbWiki['wiki']['url'];
        $data = $this->getCurl();
        $menu['menu'] = array_combine($data[1], $data[3]);
        return $menu;
    }
    /**
     * logo
     *
     * @param  string $name
     * @return string
     * create url for get logo image on the wiki of the current page
     */
    public function logo($name)
    {
        $this->paramsGet = "?action=query&prop=images&titles=" . str_replace(" ", "_", $name) . "&format=json";
        $data = $this->getCurl()['query']['pages'];
        foreach ($data as $v) {
            // var_dump($v/*['images'][0]*/);
            //     exit;
            if ($v['title'] == $name) {
                if (isset($v['images'])) {
                    $nameFile = str_replace('Fichier:', '', $v['images'][0]['title']);
                    if ($nameFile == "No-image-yet.jpg") {
                        return "none";
                    } else {
                        return str_replace('api.php', 'index.php?title=Special:Redirect/file/' . $nameFile, $this->url);
                    }
                } else {
                    return "none";
                    //$nameFile = $v['images'][0]['title']);
                }
            } else {
                return "none";
            }
        }
    }
    /**
     * logout
     *
     * @return array
     *
     */
    protected function logout()
    {
        $paramsLogout = [
            "action" => "logout",
            "format" => "json",
        ];
        $logout = $this->post($paramsLogout);
        //var_dump($logout);
        return $logout;
    }
    /**
     * edit
     *
     * @param  string $name
     * @return boolean
     * fetch text data on the wiki and rewrite with add links communecter
     * return true if success else false
     */
    public function edit($name)
    {
        // $test = $this->createAccount();
        //var_dump(getenv('WIKI_NAME'));exit;
        $this->url = $this->dbWiki['wiki']['url'];
        $test = $this->login();
        $this->paramsGet = '?action=parse&page=' . $name . '&prop=wikitext&section=0&contentmodel=wikitext&disablelimitreport=1&format=json';
        $data = $this->getCurl()['parse']['wikitext']['*'];
        if (strpos($data, "|pageCo=")) {
            return false;
        } else {
            $newText = substr($data, 0, -2) . '|pageCo=https://www.communecter.org/#@' . $_POST['currentSlug'] . ' }}';
            $arrayParams = [
                "action" => "edit",
                "title" => $name,
                "text" => $newText,
                "format" => "json",
                "nocreate" => true,
                "token" => $this->fetchToken("csrf"),
            ];
            //var_dump($arrayParams);exit;
            $verif=$this->post($arrayParams);
            if (isset($verif['error'])){
                return false;
            }
            // var_dump($verif);exit;
            $this->logout();
            return true;
        }
    }
    /**
     * login
     *
     * @return array
     * login on the wiki !!! username and password must be more secure(hash && || include db)
     */
    protected function login()
    {
        $token = $this->fetchToken("login");
        $paramsLogin = [
            "action" => "clientlogin",
            "username" => getenv('WIKI_NAME'),
            "password" => getenv('WIKI_PASS'),
            "loginreturnurl" => str_replace("/api.php", "", $this->url),
            "logintoken" => $token,
            "format" => "json",
        ];
        $login = $this->post($paramsLogin);
        return $login;
    }
    /**
     * fetchToken
     *
     * @param  string $typeToken
     * @return string
     * fetch token by type on the wiki
     */
    protected function fetchToken($typeToken)
    {
        $params = [
            "action" => "query",
            "type" => $typeToken,
            "format" => "json",
        ];
        ///condition pour wiki avec captcha (status: development)
        // if ($typeToken == "createaccount" && in_array($wiki, $this->captchaWiki)) {
        //     $params += ["meta" => "authmanagerinfo|tokens", "amirequestsfor" => "create"];
        //     $data = $this->post($params);
        //     $dataCaptchaAll = $data['query']['authmanagerinfo']['requests'][0]['fields'];
        //     $dataCaptcha = [
        //         "captchaId" => $dataCaptchaAll['captchaId']['value'],
        //         "question" => $dataCaptchaAll['captchaInfo']['value'],
        //         "token" => $data['query']['tokens']['createaccounttoken']
        //     ];
        //     return $dataCaptcha;
        // } else {
        $params += ["meta" => "tokens"];
        $dataToken = $this->post($params);
        // }

        return $dataToken['query']['tokens'][$typeToken . "token"];
    }
    /////////////////////////////
    /////////////////////////////
    //GETTERS SETTERS

    /**
     * Get the value of paramsGet
     */
    protected function getParamsGet()
    {
        return $this->paramsGet;
    }

    /**
     * Set the value of paramsGet
     *
     * @return  self
     */
    public function setParamsGet($paramsGet)
    {
        $this->paramsGet = str_replace(" ", "_", $paramsGet);
        //var_dump($this->paramsGet);//exit;
    }

    /**
     * Get the value of wiki
     */
    public function getWiki()
    {
        return $this->dbWiki;
    }

    /**
     * Set the value of wiki
     *
     * @return  void
     */
    public function setWiki($wiki)
    {
        $this->wiki = $wiki;
    }

    /**
     * Get the value of currentCat
     *
     * @return  string
     */
    public function getCurrentCat()
    {
        return $this->currentCat;
    }

    /**
     * Set the value of currentCat
     *
     * @param  string  $currentCat
     *
     */
    public function setCurrentCat($currentCat)
    {
        $this->currentCat = $currentCat;
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  void
     */
    public function setId($id)
    {
        $this->id = $id;

    }
}

//////////////////////// reserve of code
// protected function createAccount()
// {
//     var_dump("test");exit;
//     $token = $this->fetchToken("createaccount", $wiki = null);
//     $paramsToPost = [
//         "action" => "createaccount",
//         "createtoken" => $token,
//         "username" => $this->username,
//         "password" => $this->password,
//         "retype" => $this->password,
//         "createreturnurl" => $this->wikiUrl,
//         "format" => "json",
//         "cookies" => $wiki,
//     ];
//     $respCreate = $this->post($paramsToPost);
//     return $respCreate;
// }
///////////////////////variable et fonctions pour purger cache du wiki
// $paramsPurge = [
    //     "action" => "purge",
//     "generator" => "categorymembers",
//     "gcmlimit" => "max",
//     "gcmtitle" => "Category:Défi",
//     "forcelinkupdate" => "1",
//     "format" => "json"
// ];
// // "generator" => "prefixsearch",
// "titles" => "Améliorer_les_solutions_et_développer_de_nouvelles_solutions_de_mobilités_pour_tous",
// // "gpssearch" => "Utilisateur:",
// //"gapnamespace" => "0",
// $test = $this->post($paramsPurge);
// //sleep(2);
// var_dump($test);exit;
//var_dump($data);exit;
// var_dump($data['query']);
// var_dump($data['query']['results']);exit;
//$page += $data;
//////////////////////// in progress
// public function infoApi()
// {
//     $this->setParamsGet("?action=query&meta=siteinfo&format=json");
//     return $this->getCurl();
// }
// /**
//  * createAccount
//  *
//  * @param  string $wiki
//  * @return array
//  */
// /**
//  * createAccountWithCaptcha
//  *
//  * @param  string $wiki
//  * @return array
//  */
// protected function createAccountWithCaptcha($wiki, $connection, $data = null)
// {
//     if ($connection == "first") {
//         return $this->fetchToken("createaccount", $wiki);
//     } else if ($connection == "create") {
//         $respCreate = $this->post($data);
//         return $respCreate;
//     }
// }
///////////////////////////////////////////////////
