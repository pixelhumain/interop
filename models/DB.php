<?php

/**
 * InsertDB.php
 *
 * all opérations to do with db
 *
 * @author: Després Laurent <laurentd@netc.fr>
 * date: 04/2020
 */

class DB
{
    protected $url;
    protected $wiki = [];

    /**
     * __construct
     *
     * @param  string $id
     * @param  string $parentName
     * @param  string $parentType
     * @return void
     * set by parent id for new wiki or reconnect the module interop
     * set by id of wiki for data
     */
    public function __construct($id)
    {
        //var_dump(["test db construct", $id]);exit;
        //var_dump($this->setById($id));exit;
        if ($this->setById($id)) {
            //var_dump(["test db construct 1er if 1"]);//exit;
            return $this->wiki;
        } else {
            //var_dump(["test db construct 1er if 2"]);exit;
            $this->wiki = PHDB::find("mediawiki", ["parent." . $id . ".name" => $_POST['name']]);
            //var_dump($this->wiki, $_POST, $id);exit;
            if (empty($this->wiki)) {
                $this->newIn($id, $_POST['name'], $_POST['type']);
                $this->wiki = PHDB::find("mediawiki", ["parent." . $id . ".name" => $_POST['name']]);
                //var_dump(["test db construct 2eme if ", $this->wiki]);exit;
            }
            //var_dump(["test db construct else "]);
            $this->wiki = $this->mapWiki();
            //var_dump($this->wiki);exit;
            return $this->wiki;
        }
    }
    /**
     * Set the value of wiki by id
     *
     * @return  mediawiki
     */
    public function setById($id)
    {
        
        $wiki = PHDB::findOneById("mediawiki", new MongoId($id));
        if ($wiki === null) {
            //var_dump(["test db setbyid", $id, $this->wiki]);exit;
            return false;
        } else {
            $this->wiki = [$id => $wiki];
            //var_dump($this->wiki);exit;
            $this->wiki = $this->mapWiki();
            return true;
        }
    }
    /**
     * newIn
     *
     * @param  string $id
     * @param  string $name
     * @param  string $type
     * @return void
     * create base wiki
     */
    public function newIn($id, $name, $type)
    {
        $baseDataDB = [
            "name" => "wikiOf" . $name,
            "parent" => [
                $id => [
                    "type" => $type,
                    "name" => $name,
                ],
            ],
            "url" => "",
            "params" => "none",
            "logo" => "",
        ];
        Yii::app()->mongodb->selectCollection("mediawiki")->insert($baseDataDB);
        
    }
    /**
     * map value of wiki with wiki from db
     */
    public function mapWiki()
    {
        $id = key($this->wiki);
        $wiki = array_slice($this->wiki[$id], 1, null, true);
        //var_dump(['test mapwiki db' => $this->wiki]);exit;
        return ["id" => $id,
        "wiki" => $wiki];
    }
    /**
     * Set the value of wiki in db
     *
     * @return  void
     */
    public function saveWiki($wiki)
    {
        //var_dump($this->wiki); exit;
        PHDB::update("mediawiki", ["_id" => new MongoId($wiki['id'])], $wiki['wiki']);
        //var_dump(PHDB::update("mediawiki", ["_id" => $wiki['id']], $wiki['wiki'])); exit;
        $this->setById($wiki['id']);
        //var_dump($this->wiki); exit;
        return $this->wiki;
    }










    /**
     * findInCo
     *
     * @param  string $pageName
     * @return boolean
     * check if the name of the page wiki match with a slug Co
     */
    public function findInCo($pageName)
    {
        $pageToSlug = lcfirst(str_replace(" ", "", $pageName));
        $slug = PHDB::wd_remove_accents($pageToSlug);
        $verif = [];
        $match = PHDB::aggregate('slugs', array(
            array(
                '$group' => array(
                    '_id' => array('name' => '$name'),
                ),
            ),
        ));
        foreach ($match['result'] as $k => $v) {
            if (stripos($v['_id']['name'], $slug) === false) {
                continue;
            } else {
                $verif[] = $v['_id']['name'];
            }
        }
        if ($verif != []) {
            return $verif;
        } else {
            return false;
        }
    }
    /**
     * updateOne
     *
     * @param  string $champs
     * @param  mixed $value
     * @return mediawiki the new one
     * update one by key $champs to $value
     */
    public function updateOne($id, $champs, $value)
    {
        $this->wiki['wiki'][$champs] = $value;
        //var_dump($this->wiki);exit;
        PHDB::update("mediawiki", ["_id" => new MongoId($id)], $this->wiki['wiki']);
        //var_dump(PHDB::update("mediawiki", ["_id" => key($this->wiki)], $this->wiki[key($this->wiki)]));exit;
        $this->setById($id);
        return $this->wiki;
    }
    /**
     * update
     *
     * @return mediawiki the value after update in db by current state of $this->wiki
     */
    public function update()
    {
        //var_dump($this->wiki);exit;
        PHDB::update("mediawiki", ["_id" => key($this->wiki)], $this->wiki);
        return PHDB::findOne("mediawiki", array("_id" => key($this->wiki)));
        //var_dump($this->wiki);exit;
    }

    /**
     * checkExist
     *
     * @param  string $champ
     * @param  string $value
     * @return boolean
     * check if the wiki exist in db by $champ
     */
    public function checkExist($champ, $value)
    {
        $check = PHDB::findOne("mediawiki", array($champ => $value));
        if ($check === null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Get the value of url
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set the value of url
     *
     * @return  void
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }
}
