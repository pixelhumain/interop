<?php

/**
 * ApiGitlab
 *
 * @author: lotik <laurentd@netc.fr>
 * date: 05/2020
 */

class ApiGitlab
{
    /////////////////////////////
    //PROPERTIES
    /**
     * @var string
     */
    public $paramsGet = ""; //! set this before get data
    protected $url = "";
    private $project = "";
    public $tree = [];

    /////////////////////////////
    //METHODES
    public function __construct()
    {
    }
    /**
     * post
     *
     * @param  array $data
     * @return array
     */
    protected function post($data)
    {
        //$wiki = array_pop($data);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->url);

        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_COOKIEJAR, Yii::app()->getModulePath() . "/interop/cookie.txt");
        curl_setopt($curl, CURLOPT_COOKIEFILE, Yii::app()->getModulePath() . "/interop/cookie.txt");

        $ret = curl_exec($curl);

        curl_close($curl);

        $dataDecode = json_decode($ret, true);
        // var_dump($ret);
        // if (isset($dataDecode['query']['authmanagerinfo']['requests'][0])) { var_dump($dataDecode['query']['authmanagerinfo']['requests'][0]);}
        return $ret;
    }
    public function getCurl()
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->url . $this->paramsGet);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        $ret = curl_exec($curl);
        $dataDecode = json_decode($ret, true);
        curl_close($curl);
        // var_dump($ret);
        // if (isset($dataDecode['query']['authmanagerinfo']['requests'][0])) { var_dump($dataDecode['query']['authmanagerinfo']['requests'][0]);}

        return $ret;
    }
    public function pageContent($url)
    {
        $this->url = str_replace("-/blob/", "-/raw/", $url);
        $data = $this->getCurl();
        $dataNew = preg_replace_callback('/\]\(\/([\w\s\-\'\/\à\á\ä\â\è\é\ë\ê\ì\í\ï\î\ò\ó\ö\ô\ù\ú\ü\û\ñ\ç\.]+)\)/',
		function ($capture) {
			return str_replace(' ', '%20', $capture[0]);
		}, $data);
		$hostForDoc = explode("/-/", $url);
		$ret = preg_replace('/\]\(\//', ']('.$hostForDoc[0].'/-/blob/master/', $dataNew);
        return $ret;
	}

    public function tree($url, $project)
    {
        if (substr($url, -1) != '/') {
            $url .= '/';
        }
        $tree = $this->graphQlTree($url, $project, true);
        foreach ($tree as $b) {
            $blobPath = explode("/", $b['path']);
            $l = count($blobPath);
            $i = $l - 1;
            $blobToInsert = $blobPath[$i];
            if ($l === 1) {
                $this->tree[] = $b['name'];
            } else {
                while ($i > 0) {
                    $blob = [$blobPath[$i - 1] => $blobToInsert];
                    $blobToInsert = $blob;
                    $i--;
                }
                $this->tree = array_merge_recursive($this->tree, $blobToInsert);
            }
        }
        // $data = $this->graphQlTree($url, $project, false);
        // //var_dump($data['blobs']['nodes']); exit;
        // foreach ($data['blobs']['nodes'] as $k => $v) {
        //     $this->tree[] = $v['name']; 
        // }
        //https://gitlab.adullact.net/pixelhumain/codoc/-/blob/master/README.md
        //var_dump($url.str_replace("/", "%2F",$project)."/-/blob/master%2FREADME.md");
        //var_dump($this->pageContent($url.$project."/-/raw/master/README.md"));//exit;
        $this->tree += ["readme" => $this->pageContent($url.$project."/-/raw/master/README.md")];

        return $this->tree;
    }
    public function graphQlTree($url, $path, $recursive, $cursor = null)
    {
        $ret = [];
        static $data = array();
        if($recursive) {
            $paramRecur = '(recursive:true) ';
        } else {
            $paramRecur = ' ';
        }
        if($cursor == null) {
            $page = 'first';
            $pageParam = 100;
        } else {
            $pageParam = "100, after:\"".$cursor."\"";
            $page = 'first';
            //var_dump($page);exit;
        }
        $this->url = $url . "/api/graphql";
        $query = ['query' => '{
			project(fullPath: "' . $path . '") {
				repository {
					tree'.$paramRecur.'{
						blobs('.$page.':'.$pageParam.') {
                            pageInfo {
                                hasNextPage
                                endCursor
                              }
							nodes {
								name
								path
							}
						}
                    }
                }
			}
		}', ];
        $apiRet = json_decode($this->post($query), true);
        $data[] = $apiRet['data']['project']['repository']['tree']['blobs']['nodes'];
        $check = $apiRet['data']['project']['repository']['tree']['blobs']['pageInfo'];
        if ($check['hasNextPage'] === true) {
            return $this->graphQlTree($url, $path, true, $check['endCursor']);
        } else {
            foreach($data as $v) {
                $ret = array_merge($ret, $v); 
            }
            return $ret;
        }
    }

    /////////////////////////////
    //GETTERS SETTErs

    /**
     * Get the value of project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set the value of project
     *
     * @return  void
     */
    public function setProject($project)
    {
        $this->project = $project;
    }
    /**
     * Get the value of page to get data
     */
    public function getUrlPage()
    {
        return $this->url;
    }
}
/* voir si pas de de graphql
public function tree($url, $project)
{
$data = $this->graphQlTree($url, $project);//exit;
// $this->urlApi = $url."/api/v4";
// $this->apiPath = "/projects/" . $project . "/repository/tree";
// $this->urlPage = $this->urlApi . $this->apiPath;
// $data = json_decode($this->getCurl(), true);
// $tab = [];
foreach ($data as $v) {
$tabPath = explode("/",$v['path']);
$l = count($tabPath);
if ($l === 1) {
$this->tree[] = $v['name'];
} else {
$pathTab = "";
for ($i=0; $i < $l - 1; $i++) {
$pathTab = $pathTab."['".$tabPath[$i]."']";
$this->tree{$pathTab}[] = $tabPath[$i + 1];

}
}
}
// return $tab;
// $tab = [];
// foreach ($data as $v) {
//     if ($v['type'] === 'tree') {
//         $tab[] = $this->makeTree($v['path'], $v['name'], true);
//     } else {
//         $tab[] = $v['name'];
//     }
// }
var_dump($this->tree);exit;
}

public function makeTree($path, $name)
{
$ret = [$name => []];
$this->urlApi = "https://gitlab.adullact.net/api/v4";
$this->apiPath = "/projects/" . urlencode('pixelhumain/codoc') . "/repository/tree";
$this->url = $this->urlApi . $this->apiPath;
$this->paramsGet = "?path=" . urlencode($path);
$data = json_decode($this->getCurl(), true);
var_dump($data);exit;
foreach ($data as $v) {
if ($v['type'] === 'tree') {
$ret[$name] = array_merge($ret[$name], $this->makeTree($v['path'], $v['name']));
} else {
$ret[$name][] = $v['name'];
}
}
return $ret;
}
public function graphQlTree($url, $path)
{
$this->url = $url ."/api/graphql";
$query = ['query' => '{
project(fullPath: "'.$path.'") {
repository {
tree(recursive: true) {
trees {
nodes {
name
path
}
}
}
}
}
}'];
$data = $this->post($query);
return json_decode($data, true)['data']['project']['repository']['tree']['trees']['nodes'];
}
 */
