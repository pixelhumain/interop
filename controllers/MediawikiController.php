<?php

namespace PixelHumain\PixelHumain\modules\interop\controllers;

use CommunecterController, ApiMediaWiki, Yii;
/**
 * MediawikiController.php
 *
 * mediawiki interoperability
 *
 * @author: Lotik <laurentd@netc.fr>
 * Date: 02/04/2020
 */

class MediawikiController extends CommunecterController
{


    /**
     * actions
     *
     * @var array
     */
    public $dataView = [];
    private $api;

    /**
     * beforeAction
     *
     * see CommunecterController for all opérations
     *
     * @param  string $action
     * @return void
     */
    public function beforeAction($action)
    {
        //var_dump(["test controller" => [$_POST]]);exit;
        // var_dump("expression");exit;
        $this->api = new ApiMediaWiki();
        return parent::beforeAction($action);
    }
    /**
     * actionIndex
     *
     * get actions and render the views index
     * @return view
     */
    public function actionIndex()
    {
        //models
        //var_dump(["test controller" => [$_POST]]);exit;
        $this->dataView = $this->api->dbWiki;
        $wikiParams=$this->api->dbWiki;
        // var_dump($this->dataView);exit;
        if ($this->dataView['wiki']['params'] === "none") {
            if (Yii::app()->request->isAjaxRequest) {
                return $this->renderPartial("interop.views.create.index");
            }


        } else {
            if (Yii::app()->request->isAjaxRequest) {
                return $this->renderPartial("interop.views.default.indexMediaWiki",array("wikiParams"=>$wikiParams));
            }

        }
    }

    /**
     * actionChooseCategory
     *
     * @return view
     * action for save name and url of wiki then render dynForm chooseCat
     */
    public function actionChooseCategory()
    {
        // var_dump($_POST);exit;

         // $this->api = new ApiMediaWiki();
        //var_dump(["test"=> "1"]);////TODO Vérifié que le mediawiki n'existe pas deja
        //var_dump($_POST);exit
        // var_dump($this->dataView);exit;
         // var_dump("expri");exit;
        $data = $this->api->insertFirstStep($_POST['dataForm']['url'], $_POST['dataForm']['name']);
        //var_dump(['test cont' => $this->api->dbWiki]);exit;
        //$idWiki = key($this->api->dbWiki);
        // var_dump($data);exit;

        if ($data === false) {
            $this->dataView = ["error" => "Ce wiki existe deja chez nous!"];
        } else {
            $this->dataView = $this->api->dbWiki; //[/*'id' => $this->api->dbWiki['id'], "wiki" =>  $this->api->dbWiki[$idWiki]*/ $this->api->dbWiki, "data" => $data];
        }
        //var_dump(["test" => "2"]);
        // var_dump($this->dataView);exit;
        return $this->renderPartial("interop.views.create.chooseCat",array("dataView"=>$this->dataView));
    }
    /**
     * actionInsertCat
     *
     * @return view
     * save choice of categorie then render the main menu
     */
    public function actionInsertCat()
    {
        //var_dump($_POST);exit;
        $this->api->insertCat($_POST['id'], $_POST['actors'], $_POST['classifieds'], $_POST['projects']);
        $this->dataView = $this->api->dbWiki;
        return $this->renderPartial("interop.views.default.indexMediaWiki",array("wikiParams"=>$this->dataView));
    }

    /**
     * actionMenuLeft
     *
     * @return view
     * view menuleft by search bar or category button
     */
    public function actionMenuLeft()
    {

        //models
        if ($_POST['categorie'] == 'search') {
            $this->api->dbWiki += $this->api->search($_POST['page']);
            $this->dataView = $this->api->dbWiki;
        } else {
            $this->api->dbWiki += $this->api->menu();
            $this->dataView = $this->api->dbWiki;
        }
        //view
        return $this->renderPartial("interop.views.menus.pages",array("dataView"=>$this->dataView));
    }

    /**
     * actionPage
     *
     * @return view
     *
     */
    public function actionPage()
    {
        //models
        $this->api->dbWiki += $this->api->page($_POST['page']);
        $this->dataView = $this->api->dbWiki;
        //view
        // var_dump($this->dataView);exit;
        return $this->renderPartial("interop.views.page.index",array("dataView"=>$this->dataView));
    }

    /**
     * actionEdit
     *
     * @return string
     * route for edit wiki witha communecter link
     */
    public function actionEdit()
    {
        if ($this->api->edit($_POST['page'])) {
            return '<p> Le wiki a bien été édité</p>';
        } else {
            return '<p> Un probléme est survenu veuillez contacter l\'administrateur</p>';
        }
    }
    public function actionDoc()
    {
        header("Location: https://gitlab.adullact.net/pixelhumain/codoc/-/blob/master/4%20-%20Documentation%20technique/mediawiki.md");
    }
    public function actionCreate()
    {
        $this->dataView = $this->api->createAccount();
    }
}
