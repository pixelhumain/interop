<?php

namespace PixelHumain\PixelHumain\modules\interop\controllers\actions;
use CAction;
use CO2Stat;
use Yii;

class IndexAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
    	//echo "Hello there"; exit;
      	CO2Stat::incNbLoad("co2-interop");
		if(Yii::app()->request->isAjaxRequest)
			return $this->getController()->render("interop.views.co.index");
		else {
			$this->getController()->layout = "//layouts/empty";
			//$controller->render( "interop.views.co.index");
			return $this->getController()->render("interop.views.co.index");
		}

    }
}