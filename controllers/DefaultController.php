<?php

namespace PixelHumain\PixelHumain\modules\interop\controllers;

use CommunecterController;
use Yii;

/**
 * DefaultController.php
 *
 * OneScreenApp for Communecting people
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 * @contrib: Després Laurent <laurentd@netc.fr>
 * date: 03/2020
 */


class DefaultController extends CommunecterController
{

	/**
	 * actions
	 *
	 * @var array
	 */
	public $actions = [];

	/**
	 * beforeAction
	 *
	 * see CommunecterController for all opérations
	 * 
	 * @param  string $action
	 * @return void
	 */
	public function beforeAction($action)
	{
		//parent::initPage();
		return parent::beforeAction($action);
	}
	/**
	 * getPossiblesActions
	 *
	 * return array of data for first page
	 * 
	 * @return array
	 */
	public function getPossiblesActions($name = "")
	{
		$possiblesActions = array(
			"poleEmploi" 			=> 	[
				"logo" => "logo_pole_emploi.png",
				"themes" => ["Emplois"],
				"mediaWiki" => false,
				"create" => false
			],
			// "lotik"/*wiki local*/	=> 	[
			// 	"logo" => "lotik-logo.png",
			// 	"themes" => ["Documentations"],
			// 	"mediaWiki" => true,
			// 	"create" => true
			// ],
			// "movilab" 				=>	[
			// 	"logo" => "logo-movilab2.png",
			// 	"themes" => ["Acteurs", "Projets", "Documentations"],
			// 	"mediaWiki" => true,
			// 	"create" => true
			// ]
		);
		if ($name != "") {
			return [$name => $possiblesActions[$name]];
		} else {
			return $possiblesActions;
		}
	}
	/**
	 * actionIndex
	 *
	 * get actions and render the views index
	 * @return void
	 */
	public function actionIndex()
	{
		$this->actions = $this->getPossiblesActions();
		if (Yii::app()->request->isAjaxRequest)
			return $this->renderPartial("index");
		else {
			return $this->render("index");
		}
	}

	/**
	 * actionIndex
	 *
	 * get actions and render the views index
	 * @return void
	 */
	public function actionMediaWiki()
	{
		$this->actions = $this->getPossiblesActions($_POST['name']);
		if (Yii::app()->request->isAjaxRequest)
			return $this->renderPartial("indexMediaWiki");
		else {
			return $this->render("indexMediaWiki");
		}
	}

	public function actionDoc()
	{
		//header("Location: https://loolood.fr/stage/DocAtFormatHtml/");
		$this->redirect("/code/modules/interop/controllers/InsertDBController.php");
	}
}
