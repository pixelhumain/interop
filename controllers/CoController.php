<?php

namespace PixelHumain\PixelHumain\modules\interop\controllers;

use CommunecterController;

/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class CoController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
	        'index'   => \PixelHumain\PixelHumain\modules\interop\controllers\actions\IndexAction::class,
	    );
	}
}
