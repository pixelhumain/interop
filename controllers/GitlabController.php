<?php

namespace PixelHumain\PixelHumain\modules\interop\controllers;

use CommunecterController, ApiGitlab, Rest;
/**
 * MediawikiController.php
 *
 * mediawiki interoperability
 *
 * @author: Lotik <laurentd@netc.fr>
 * Date: 05/2020
 */

class GitlabController extends CommunecterController
{

	/**
	 * api
	 *
	 * @var object
	 */
	//public $dataView = [];
	private $api;

	/**
	 * beforeAction
	 *
	 * see CommunecterController for all opérations of parent::beforeAction()
	 * and init obj ApiGitlab();
	 * 
	 * @param  string $action
	 * @return void
	 */
	public function beforeAction($action)
	{
		////$_POST must have min index['id','name','type','categorie] with set value
		//var_dump($_POST);exit;
		//var_dump("plop");
		$this->api = new ApiGitlab();
		return parent::beforeAction($action);
	}
	/**
	 * actionIndex
	 *
	 * get actions and render the views index
	 * @return view
	 */
	public function actionIndex()
	{
	}


	/**
	 * actionPage
	 *
	 * @return view
	 */
	public function actionPage()
	{
		//var_dump($_GET);
		$data = $this->api->pageContent($_GET['url']);
		//var_dump($data); exit;
		return Rest::json(["content" => $data]);
	}
	/**
	 * actionTree
	 *
	 * @return view
	 */
	public function actionTree()
	{

		//var_dump($_POST);exit;
		$data = $this->api->tree($_GET['url'], $_GET['repo']);
		//var_dump($data);exit;
		return Rest::json($data);
	}

}
