$(document).ready(function () {
    $("#menu-left").show();
    $('#loading-menu').hide();
    $("#accordion>li").click(function () {
        $("#accordion>li").removeClass("active");
        if ($(this).attr("aria-expanded") == "false") {
            $(this).addClass("active");
            $("#accordion>li>div>ul").attr("aria-expanded", "false");
            //$("#accordion>li>div").setAttribute("aria-expanded", false)
        }
    });
    $("#dropdown_search").html("");
    $(".link-page").click(function () {
        // $("#menu-left").hide();
        var id = $(this).data("id");
        var name = $(this).data("name");
        var categorie = $(this).data("categorie");
        var pageName = $(this).data("page");
        interopMW.initObj(id, name, categorie);
        interopMW.pageContent(pageName);
        interopMW.showLoader('page');
        $('#loading-page>p').text(pageName);
    });
});