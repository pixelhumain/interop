function show(id) {
	$(".btn-actor").removeAttr("style");
	$("." + id).attr("style", "background-color:#65BA91");
	$("#body-actors").show();
	$("#subContent").html("").hide();
	$(".c-actor").hide();
	$("#" + id).show();
}
$(document).ready(function () {
	$('#loading-page').hide();
	$('#block-interop').show();
	$(".c-actor").hide();
	$("#btw").show();
	$(".btw").attr("style", "background-color:#65BA91");
	$('#loading-page').hide();
	$(".btn-sm").click(function () {
		$("#body-actors").hide();
		$("#subContent").show();
		interopMW.mediaWiki.categorie = $(this).data("categorie");
		if ($(this).data("categorie") == "utilisateurs") {
			pageName = "Utilisateur:" + $(this).text();
		} else {
			pageName = $(this).text();
		}
		$('#loading-page>p').text($(this).text());
		interopMW.showLoader('page');
		interopMW.pageContent(pageName);
	});
	$("#edit-wiki").click(function () {
		interopMW.mediaWiki.currentSlug = $(this).data('slug');
		$(this).remove();
		$("#mark-on-wiki").load(baseUrl+"/interop/mediawiki/edit", interopMW.mediaWiki);
	});
	$("#create-in-co").click(function () {
		var data = {};
		var typeForm = $("#create-in-co").data('type');
		console.log(typeForm);
		if (typeForm === 'ressources') {
			data.section = "offer";
			// data.category = "service";
			// data.subtype = "househelp";
			// data.type = "ressources";
		}
		if (typeof $('#imageEl').attr("src") != undefined) {
			data.image = $('#imageEl').attr("src");
		}
		if (typeof $('#typeEl').data("type") != undefined) {
			data.type = $('#typeEl').data("type");
		}
		if (typeof $('#nameEl').text() != undefined) {
			data.name = $('#nameEl').text();
		}
		if (typeof $('#shortEl').text() != undefined) {
			data.shortDescription = $('#shortEl').text();
		}
		if (typeof $('.tagsEl').text() != undefined) {
			let tags = [];
			$('.tagsEl').each(function () {
				tags.push($(this).text());

			});
			data.tags = tags;
		}
		if (typeof $('#urlEl').attr("href") != undefined) {
			data.url = $('#urlEl').attr("href");
		}

		//if (typeForm === 'projets') {
		dyFObj.openForm(typeForm, null, data);

		// if (typeForm === 'ressources') {
		// dyFObj.openForm(typeForm, null, null);
		// }
		// else{
		// dyFObj.openForm(typeForm, null, data);	
		// }
	});
});