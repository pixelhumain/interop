
//var actorsData = classifiedsData = projectsData = [];
$(document).ready(function () {
    var dataForm = [];
    var actorsData = [];
    var classifiedsData = [];
    var projectsData = [];
    var id = document.getElementById("idWiki").getAttribute('data-value');
    document.querySelectorAll("#data-block>div[data-name=acteurs]").forEach(function (element) { actorsData.push($(element).data('value')) });
    document.querySelectorAll("#data-block>div[data-name=ressources]").forEach(function (element) { classifiedsData.push($(element).data('value')) });
    document.querySelectorAll("#data-block>div[data-name=projets]").forEach(function (element) { projectsData.push($(element).data('value')) });
    var insertCatWikiForm = {
        jsonSchema: {
            title: "Catégorie wiki par type: ",
            icon: "fa-at",
            properties: {
                acteurs: dyFInputs.tags(null,null,"Catégorie acteurs:"),
                classifieds: dyFInputs.tags(null,null,"Catégorie resources:"),
                projects: dyFInputs.tags(null,null,"Catégorie projets:"),
            }
        },
        save: async function () {
            await $.each(dyFObj.elementObj.dynForm.jsonSchema.properties, function (k, val) {
                dataForm[k] = $("#" + k).val();
            });
            console.log(dataForm)
            //$("#central-container").load("interop/mediawiki/insert", { name: contextName, id: contextId, type: contextType, dataForm });
            ajaxPost(".central-section", 
                     baseUrl+"/interop/mediawiki/insertCat",
                     {  name: "",
                        id: id,
                        type: "",
                        actors: dataForm.acteurs,
                        classifieds: dataForm.classifieds,
                        projects: dataForm.projects,
                        },
                     null,
                     "html");
                     dyFObj.closeForm();

           
        }
    };
    var dataUpdate = {acteurs: actorsData};
    dataUpdate.classifieds = classifiedsData;
    dataUpdate.projects = projectsData;
    $("#subDyn").html(dyFObj.openForm(insertCatWikiForm,"initUpdateInfo", dataUpdate));
    $("#btn-submit-form").click(function () {
        insertCatWikiForm.save()
    });
});

