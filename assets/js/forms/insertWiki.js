insertWikiForm = {
    jsonSchema: {
        title: "Votre MediaWiki",
        icon: "fa-at",
        properties: {
            name: {
                inputType: "text",
                label: "Nom du wiki",
                placeholder: "wiki_name",
                rules: { required: true }
            },
            url: {
                inputType: "text",
                label: "Url du wiki",
                placeholder: "http(s)://HOST/nameOfWiki",
                rules: { required: true }
            },
            parentType: {
                inputType: "hidden",
                value: contextType
            },
            parentName: {
                inputType: "hidden",
                value: contextName
            },
            parentId: {
                inputType: "hidden",
                value: contextId
            },
        }
    },
    save: function () {
        var today = new Date();
        var dataForm = { date: today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear(), collection: "mediawiki" };
        $.each(dyFObj.elementObj.dynForm.jsonSchema.properties, function (k, val) {
            dataForm[k] = $("#" + k).val();
        });
        ajaxPost($("#body-doc-content"),
                    baseUrl+"/interop/mediawiki/chooseCategory", { name: contextName, id: contextId, type: contextType, dataForm: dataForm },null, "html")
        dyFObj.closeForm();
    }
};

$("#central-section").html(dyFObj.openForm(insertWikiForm));
$("#btn-submit-form").click(function () {

    insertWikiForm.save()
});


