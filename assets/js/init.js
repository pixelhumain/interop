/* ******************
CO.js
********************* */
var interopObj = {
	osm : {
		name : "Wikidata",
		color : "grey",
		icon : "group",
		type : "wiki",
		urlImg : modules.interop.assets +"/images/logos/logo-osm.png",
		paramsUrl : {
			// cityFields : ["wikidataID"],
			// others : ["textSearch"]
		},
		getUrlApi : function(params){
			var url_osm = baseUrl + '/api/convert/osm?url=http://overpass-api.de/api/interpreter?data=[out:json];node["name"](poly:"'+geoShape+'");out%20'+endNow+';';

		    // if (amenity_filter == null) {
		    //     if (text_search_name !== "") {
				//         url_osm = baseUrl + '/api/convert/osm?url=http://overpass-api.de/api/interpreter?data=[out:json];node["name"~"'+text_search_name+'",i](poly:"'+geoShape+'");out%20'+endNow+';';
		    //     } 
		    // } else {
		    //     if (amenity_filter == "") {
		    //         url_osm = baseUrl + '/api/convert/osm?url=http://overpass-api.de/api/interpreter?data=[out:json];node["amenity"="NIMPORTEQUOI"](poly:"'+geoShape+'");out%20'+endNow+';';
		    //     } else {
		    //         if (text_search_name == "") {
		    //             url_osm = baseUrl + '/api/convert/osm?url=http://overpass-api.de/api/interpreter?data=[out:json];node["amenity"~"^('+amenity_filter+')"](poly:"'+geoShape+'");out%20'+endNow+';';
		    //         } else {
		    //             url_osm = baseUrl + '/api/convert/osm?url=http://overpass-api.de/api/interpreter?data=[out:json];node["name"~"'+text_search_name+'"]["amenity"~"^('+amenity_filter+')"](poly:"'+geoShape+'");out%20'+endNow+';';
		    //         }
		    //     }
		    // }  
			var url = baseUrl + '/api/convert/osm?url=http://overpass-api.de/api/interpreter?data=[out:json];node["name"](poly:"'+geoShape+'");out%20'+endNow+';';
			if (params.textSearch !== "")
			url += "&text_filter="+params.textSearch;
			return url;
		}
	},
	wikidata : {
		name : "Wikidata",
		color : "grey",
		icon : "group",
		type : "wiki",
		urlImg : modules.interop.assets +"/images/logos/logo-wikidata.png",
		paramsUrl : {
			cityFields : ["wikidataID"],
			others : ["textSearch"]
		},
		getUrlApi : function(params){
			var url = baseUrl + "/api/convert/wikipedia?url=https://www.wikidata.org/wiki/Special:EntityData/"+params.wikidataID+".json";
			if (params.textSearch !== "")
			url += "&text_filter="+params.textSearch;
			return url;
		}
	},
	poleEmploi : {
		name : "Pole Emploi",
		color : "grey",
		icon : "group",
		type : "poleEmploi",
		urlImg : modules.interop.assets +"/images/logos/logo_pole_emploi.png",
		paramsUrl : {
			cityFields : ["insee"],
			others : ["indexMax"]
		},
		getUrlElement : function(params){
			var url = params.url;
			return url;
		},
		getUrlApi : function(params){
			var url = baseUrl + "/api/convert/poleemploi?url=https://api.emploi-store.fr/partenaire/offresdemploi/v1/rechercheroffres";
			return url;
		},
		getParamsUrl : function(objType){
			var data = {
				'technicalParameters'  : {
					'page' : 1,
					'per_page' : 30,
					'sort' : 1
				},
				'criterias' : {}
			} ;

			var listScope = interop.getScope();

			mylog.log("listScope", listScope);
			var dataCities = null ;
			$.each(listScope,function(e,v){
				dataCities = interop.getCityDataById(v.id, v.type, objType.paramsUrl.cityFields);
			});
			
			if(dataCities != null && typeof dataCities.insee != "undefined" )
				data["criterias"]["cityCode"] = dataCities.insee;
				
			if(typeof searchObject != "undefined" && searchObject.text != "")
				data["criterias"]["keywords"] = searchObject.text;
				
			if(typeof searchObject != "undefined" && searchObject.subType != "" && typeof modules.jobs.categories.subcat[searchObject.subType] != "undefined")
				data["criterias"]["largeAreaCode"] = modules.jobs.categories.subcat[searchObject.subType].poleEmploiKey;
				
				if(typeof searchObject != "undefined" && searchObject.priceMin != "")
				data["criterias"]["minSalary"] = searchObject.priceMin;
				
				return data ;
			},
		startSearch : function(indexMin, indexStep){
			interop.currentType = ["poleEmploi"]
			interop.startSearch(indexMin, indexStep);
		}
	},
};

function initRangeInterop(){
	// $.each(interopObj, function(key, value){
		// 	searchObject.ranges[key] = { indexMin : 0, indexMax : 30, waiting : 30 }
	// });
}

function initHeaderParams(){
	$.each(interopObj, function(key, value){
		headerParams[key] = { color: value.color, icon: value.icon, name: value.name }
	});
}


function initTypeObj(){
	$.each(interopObj, function(key, value){
		typeObj[key] = { col: key, ctrl: key, color: value.color, icon: value.icon, sameAs:key }
	});
}

initHeaderParams();
initTypeObj();
// typeObj[typeObj[addType].sameAs].ctrl

// typeObj.interop = {
// 	col:"interop", 
// 	ctrl:"interop", 
// 	icon : "group", 
// 	titleClass : "bg-green",
// 	color:"green",
// 	bgClass : "bgOrga"
// } ;




// if(typeof searchObject == "undefined"){
	// 	lazyLoad( moduleUrl+'/js/default/search.js', null, function(){
		// 		searchAllEngine.initRanges();
// 		initRangeInterop();

// 	});
// }else if(typeof searchObject.ranges == "undefined"){
// 	searchAllEngine.initRanges();
// 	initRangeInterop();
// }else if( typeof searchObject.ranges.interop == "undefined" ){
	// 	initRangeInterop();
// }


