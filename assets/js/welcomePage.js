
$(document).ready(function () {
    $("#menu-left").hide();
    $('#loading-menu').hide();
    $('#loading-page').hide();
    $(".btn-directory-categorie").click(function () {
        $(".central-section").removeClass("col-md-9 col-sm-9 col-lg-10").addClass("col-xs-12");
        let titleLoading = ($(this).data("categorie") === "actors") ? "acteurs" : "";
        titleLoading = ($(this).data("categorie") === "classifieds") ? "ressources" : titleLoading;
        titleLoading = ($(this).data("categorie") === "projects") ? "projets" : titleLoading;
        $("#menu-left").hide();
        $('#loading-menu>p').text(titleLoading);
        interopMW.showLoader('menu');

        var name = $(this).data("name");
        var categorie = titleLoading;
        var id = $(this).data("id");
        $('.btn-directory-categorie').removeAttr('style');
        $(this).attr('style', 'background-color:#65BA91');

        interopMW.initObj(id, name, categorie);
        interopMW.initMenu();
    });
    $("#close-interop").click(function () {
        $("#menu-left").hide();
        $("#menu-left-container").show();
        $(".central-section").removeClass("col-xs-12").addClass("col-md-9 col-sm-9 col-lg-10");
        //urlCtrl.loadByHash('#@'+contextData.slug);
        pageProfil.views.mediawiki();
    });
    $("#search-in-wiki").click(function () {
        var name = $(this).data("name");
        var categorie = $(this).data("categorie");
        var id = $(this).data("id");
        var searchText = $("#search-bar-wiki>input").val();
        $('#loading-menu>p').text(searchText);
        interopMW.showLoader('menu');
        if (searchText) {
            interopMW.initObj(id, name, categorie);
            interopMW.search(searchText);
        }
    });
    function show(id) {
        $(".btn-actor").removeAttr("style");
        $("." + id).attr("style", "background-color:#65BA91");
        $("#body-actors").show();
        $("#subContent").html("").hide();
        $(".c-actor").hide();
        $("#" + id).show();
    }
});

